import React,{useState} from 'react';
import axios from "axios";
import './App.css';

function App() {
  const [pokemonName, setPokemonName] = useState("");
  const [pokemonChosen,setPokemonChosen] = useState(false);
  const [pokemon, setPokemon] = useState({
        name: "",
        number: "",
        species: "",
        image: "",
        hp: "",
        attack: "", 
        defense: "",
        speed: "",
				type: "",
  });

  const searchPokemonAll = () =>{
    axios.get(`https://pokeapi.co/api/v2/pokemon?limit=150`)
    .then((res)=>{
      console.log(res.data)
    });
  };

  const searchPokemon = () =>{
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
    .then((res)=>{
      // console.log(res.data)
      setPokemon({
        name: pokemonName,
        number: res.data.id,
        species: res.data.species.name,
        image: res.data.sprites.front_default,
        hp: res.data.stats[0].base_stat,
        attack: res.data.stats[1].base_stat,
        defense: res.data.stats[2].base_stat,
        speed: res.data.stats[5].base_stat,
				type: res.data.types[0].type.name,
      });
      setPokemonChosen(true);
    });
  };


  return (
    <div className="App">
      <div className="App-header">
        <img src="https://fontmeme.com/permalink/211012/d3242ff2ed2026a6fc8b3e691de9ed72.png" alt="pokedex"/>
        <nav>
          <button onClick={searchPokemonAll}>All Pokemon</button>
          <button onClick={searchPokemon} >Search Pokemon</button>
          <input type="text" onChange={(e)=>{setPokemonName(e.target.value)}}/>
        </nav>
      </div>
      <main>
        <div className="Card-Pokemon">
          {!pokemonChosen ? (
            <h1>Please choose a Pokemon</h1>
          ):(
            <>
              <h1>{pokemon.name}</h1>
              <img src={pokemon.image} alt={pokemon.name}/>
              <h3>#{pokemon.number}</h3>
              <h3>Species: {pokemon.species}</h3>
              <h3>Type: {pokemon.type}</h3>
              <p>HP: {pokemon.hp}</p>
              <p>Attack: {pokemon.attack}</p>
              <p>Defense: {pokemon.defense}</p>
              <p>Speed: {pokemon.speed}</p>
            </>
          )}
        </div>
      </main>

      
    </div>
  );
}

export default App;
